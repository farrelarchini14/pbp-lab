1. Apakah perbedaan antara JSON dan XML?
    
    JSON
-> Merupakan JavaScript object notation
-> Didasarkan pada bahasa JavaScript.
-> File sangat mudah dibaca dibandingkan dengan XML.
-> Bisa menggunakan array.
-> Mempunyai tipe data yang terdefinisi.
-> Menggunakan UTF-8 encoding saja.
-> Cenderung menggunakan tipe data teks dan numerik, termasuk bilangan bulat dan string.

    XML
-> Merupakan Extensible markup language
-> Didasarkan dari SGML (Standard Generalized Markup Language)
-> Lebih sulit dibaca dan di interpret
-> Tidak support array
-> Datanya typeless
-> Menggunakan beberapa jenis encoding
-> Support tipe data seperti angka, text, gambar, tabel, grafik, dll

Referensi = 
https://www.guru99.com/json-vs-xml-difference.html
https://www.geeksforgeeks.org/difference-between-json-and-xml/



2. Apakah perbedaan antara HTML dan XML?
    HTML
-> Merupakan Hyper Text Markup Language
-> Tidak case sensitive
-> Tipe datanya static
-> HTML tidak memerlukan closing tags
-> Digunakan untuk menampilkan data
-> Bila terjadi sedikit error maka akan diabaikan

    XML
-> Merupakan extensible Markup Language
-> Case sensitive
-> Tipe datanya dynamic
-> XML memerlukan closing tags
-> Digunakan untuk menyimpan data
-> Tidak boleh ada error




